#include <iostream>
#include "widget.h"
#include "ui_widget.h"
#include "Pi_Serial.h"

using namespace std;
int fd;
QByteArray cnnn;
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    //设置滑动条控件的最小值
    ui->cmd1->setMinimum(0);
    ui->cmd2->setMinimum(0);
    ui->cmd3->setMinimum(0);
    ui->cmd4->setMinimum(0);
    ui->cmd5->setMinimum(0);
    //设置滑动条控件的最大值
    ui->cmd1->setMaximum(180);
    ui->cmd2->setMaximum(180);
    ui->cmd3->setMaximum(180);
    ui->cmd4->setMaximum(180);
    ui->cmd5->setMaximum(180);

    ui->cmd1->setPageStep(5);
    ui->cmd2->setSingleStep(5);
    ui->cmd3->setSingleStep(5);
    ui->cmd4->setSingleStep(5);
    ui->cmd5->setSingleStep(5);
    //设置滑动条控件的值
    ui->cmd1->setValue(90);
    ui->cmd2->setValue(90);
    ui->cmd3->setValue(90);
    ui->cmd4->setValue(90);
    ui->cmd5->setValue(90);
    connect(ui->cmd1, SIGNAL(valueChanged(int)), this, SLOT(on_cmd1_valueChanged(int)));
    connect(ui->cmd2, SIGNAL(valueChanged(int)), this, SLOT(on_cmd2_valueChanged(int)));
    connect(ui->cmd3, SIGNAL(valueChanged(int)), this, SLOT(on_cmd3_valueChanged(int)));
    connect(ui->cmd4, SIGNAL(valueChanged(int)), this, SLOT(on_cmd4_valueChanged(int)));
    connect(ui->cmd5, SIGNAL(valueChanged(int)), this, SLOT(on_cmd5_valueChanged(int)));
    connect(ui->pushButton_1, SIGNAL(clicked(void)), this, SLOT(on_pushButton_1_clicked()));
    connect(ui->pushButton_2, SIGNAL(clicked(void)), this, SLOT(on_pushButton_2_clicked()));
    connect(ui->pushButton_3, SIGNAL(clicked(void)), this, SLOT(on_pushButton_3_clicked()));
    connect(ui->pushButton_4, SIGNAL(clicked(void)), this, SLOT(on_pushButton_4_clicked()));
    connect(ui->pushButton_5, SIGNAL(clicked(void)), this, SLOT(on_pushButton_5_clicked()));
    connect(ui->pushButton_6, SIGNAL(clicked(void)), this, SLOT(on_pushButton_6_clicked()));

    Pi_Serial *pi_serial = new Pi_Serial();

    pi_serial->serial_open("/dev/tty.wchusbserial141120");
    if (fd ==0 || fd == -1) {
        cout<<"fd\t"<<fd<<endl;
        cout<<"error"<<endl;
        exit(0);
    }
    cout<<"fd\t"<<fd<<endl;

//    pi_serial->serial_write("ff020101f4");



    int data = PER * 90 + 500;
    cout<<data<<endl;


    int cmd = 1500;
    int cmd_l = cmd >> 8;
    int cmd_r = cmd & (0xff);
    cout<<"cmd_l"<<cmd_l<<endl;
    cout<<"cmd_r"<<cmd_r<<endl;
    char cmd_l_array[4],cmd_r_array[4];
    sprintf(cmd_l_array,"%x", cmd_l);
    sprintf(cmd_r_array,"%x", cmd_r);
    cout<<"cmd_l_array"<<cmd_l_array<<endl;
    cout<<"cmd_r_array"<<cmd_r_array<<endl;


    char cmd_16[4];
    sprintf(cmd_16, "%#x", cmd);

    cout<<"cmd_16\t"<<cmd_16<<endl;
    cout <<"cmd16[1]" <<cmd_16[1] <<endl;
    cout <<"cmd16[2]" <<cmd_16[2] <<endl;

    char data_r[3] = {cmd_16[1], cmd_16[2]};
    char data_l[2] = {cmd_16[0]};
    cout<<"test\t"<< data_r<<endl;
    char *data_r_str = data_r;
    char *data_l_str = data_l;

    char data_r_0x[100]="";
    strcpy(data_r_0x, "0x");
    strcat(data_r_0x, data_r_str);

    char data_l_0x[100]="";
    strcpy(data_l_0x, "0x0");
    strcat(data_l_0x, data_l_str);
    cout<<"data_l_0x\t" << data_l_0x<<endl;
    cout<< "data_r_0x\t"<<data_r_0x<<endl;

    char num[] = "1";
    char data_ch[100] = "";
    strcpy(data_ch, "0x0");
    strcat(data_ch, num);
    cout<<"ch\t"<<data_ch<<endl;

    char command[100] = "";
    strcat(command, "0xff");
    strcat(command, "0x02");
    strcat(command, data_ch);
    strcat(command, data_l_0x);
    strcat(command, data_r_0x);

    cout << "commmand\t" << command << endl;

    atoi(command);
    cout << "commmand\t" << command << endl;

    write(fd, "", 5);
    cnnn.append(command);

}

Widget::~Widget()
{
    delete ui;
}



void Widget::on_cmd1_valueChanged(int value)
{
    int pos = ui->cmd1->value();
    QString str = QString("%1").arg(pos);
    ui->lineEdit_1->setText(str);

    int data = PER * pos + 500;
    cout<<data<<endl;
    unsigned char h=data>>8&0xff;
    unsigned char l=data&0xff;
    cout<<l<<endl;
    cout<<h<<endl;

    QByteArray cnm;
    cnm.resize(5);
    cnm[0]=0xff;
    cnm[1]=0x02;
    cnm[2]=0x00;
    cnm[3]=l;
    cnm[4]=h;
    write(fd, cnm, 5);
}

void Widget::on_cmd2_valueChanged(int value)
{
    int pos = ui->cmd2->value();
    QString str = QString("%1").arg(pos);
    ui->lineEdit_2->setText(str);

    int data = PER * pos + 500;
    cout<<data<<endl;
    unsigned char h=data>>8&0xff;
    unsigned char l=data&0xff;
    cout<<l<<endl;
    cout<<h<<endl;

    QByteArray cnm;
    cnm.resize(5);
    cnm[0]=0xff;
    cnm[1]=0x02;
    cnm[2]=0x01;
    cnm[3]=l;
    cnm[4]=h;
    write(fd, cnm, 5);

}

void Widget::on_cmd3_valueChanged(int value)
{
    int pos = ui->cmd3->value();
    QString str = QString("%1").arg(pos);
    ui->lineEdit_3->setText(str);

    int data = PER * pos + 500;
    cout<<data<<endl;
    unsigned char h=data>>8&0xff;
    unsigned char l=data&0xff;
    cout<<l<<endl;
    cout<<h<<endl;

    QByteArray cnm;
    cnm.resize(5);
    cnm[0]=0xff;
    cnm[1]=0x02;
    cnm[2]=0x02;
    cnm[3]=l;
    cnm[4]=h;
    write(fd, cnm, 5);
}

void Widget::on_cmd4_valueChanged(int value)
{
    int pos = ui->cmd4->value();
    QString str = QString("%1").arg(pos);
    ui->lineEdit_4->setText(str);

    int data = PER * pos + 500;
    cout<<data<<endl;
    unsigned char h=data>>8&0xff;
    unsigned char l=data&0xff;
    cout<<l<<endl;
    cout<<h<<endl;

    QByteArray cnm;
    cnm.resize(5);
    cnm[0]=0xff;
    cnm[1]=0x02;
    cnm[2]=0x03;
    cnm[3]=l;
    cnm[4]=h;
    write(fd, cnm, 5);
}

void Widget::on_cmd5_valueChanged(int value)
{
    int pos = ui->cmd5->value();
    QString str = QString("%1").arg(pos);
    ui->lineEdit_5->setText(str);

    int data = PER * pos + 500;
    cout<<data<<endl;
    unsigned char h=data>>8&0xff;
    unsigned char l=data&0xff;
    cout<<l<<endl;
    cout<<h<<endl;

    QByteArray cnm;
    cnm.resize(5);
    cnm[0]=0xff;
    cnm[1]=0x02;
    cnm[2]=0x04;
    cnm[3]=l;
    cnm[4]=h;
    write(fd, cnm, 5);
}

void Widget::on_pushButton_1_clicked()
{


}

void Widget::on_pushButton_2_clicked()
{

}

void Widget::on_pushButton_3_clicked()
{
    int pos1 = 60,pos2 = 90, pos3 = 90;

    int data1 = PER * pos1 + 500;
    int data2 = PER * pos2 + 500;
    int data3 = PER * pos3 + 500;

    unsigned char h1=data1>>8&0xff;
    unsigned char l1=data1&0xff;

    unsigned char h2=data2>>8&0xff;
    unsigned char l2=data2&0xff;

    unsigned char h3=data3>>8&0xff;
    unsigned char l3=data3&0xff;


    QByteArray cnm1,cnm2,cnm3;
    cnm1.resize(5);
    cnm2.resize(5);
    cnm3.resize(5);
    cnm1[0]=0xff;
    cnm1[1]=0x02;
    cnm1[2]=0x00;
    cnm1[3]=l1;
    cnm1[4]=h1;

    cnm2[0]=0xff;
    cnm2[1]=0x02;
    cnm2[2]=0x01;
    cnm2[3]=l2;
    cnm2[4]=h2;

    cnm3[0]=0xff;
    cnm3[1]=0x02;
    cnm3[2]=0x02;
    cnm3[3]=l3;
    cnm3[4]=h3;
    write(fd, cnm1, 5);
    write(fd, cnm2, 5);
    write(fd, cnm3, 5);
}

void Widget::on_pushButton_4_clicked()
{
    int year = 452;

    QByteArray cnm;
    cnm.resize(5);
    cnm[0]=0xff;
    cnm[1]=0x02;
    cnm[2]=0x02;
    cnm[3]=char(year >>8);
//    cnm[4]=low;
    qDebug("ok");
//    write(fd, cnm, 5);
    write(fd, cnnn, 5);
}

void Widget::on_pushButton_5_clicked()
{
    int year = 9000;
    int high = year >> 8;
    int low = year & 0xff;
    printf("0x%x", high);
    printf("0x%x", low);
}

void Widget::on_pushButton_6_clicked()
{
    int cmd = 1500;
    int cmd_l = cmd >> 8;
    int cmd_r = cmd & (0xff);
    cout<<"cmd_l"<<cmd_l<<endl;
    cout<<"cmd_r"<<cmd_r<<endl;
    char cmd_l_array[4],cmd_r_array[4];
    sprintf(cmd_l_array,"%x", cmd_l);
    cout<<cmd_l_array[0]<<endl;
    cout<<cmd_l_array[1]<<endl;
    cout<<cmd_l_array[2]<<endl;
    cout<<cmd_l_array[3]<<endl;
    sprintf(cmd_r_array,"%x", cmd_r);
    cout<<cmd_r_array[0]<<endl;
    cout<<cmd_r_array[1]<<endl;
    cout<<cmd_r_array[2]<<endl;
    cout<<cmd_r_array[3]<<endl;
    QByteArray cnm;
    cnm.resize(5);
    cnm[0]=0xff;
    cnm[1]=0x02;
    cnm[2]=0x02;
    cnm[3]=cmd_l_array[1];
//    cnm[4]=
}
