//
// Created by 韩懿恺 on 2018/4/25.
//

#ifndef RASPBIAN_PI_SERIAL_H
#define RASPBIAN_PI_SERIAL_H

#include "stdio.h"
#include <cstdio>
#include <cstdlib>
#include "stdlib.h"
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <pthread.h>
#include <dirent.h>
#include <ctype.h>

#define PER 11.1111
class Pi_Serial {
public:
    void serial_open(const char *path);
    const char* serial_close(int fd);
    void serial_write(char *cmd);
};


#endif //RASPBIAN_PI_SERIAL_H
