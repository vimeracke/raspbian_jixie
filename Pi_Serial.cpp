//
// Created by 韩懿恺 on 2018/4/25.
//
extern int fd;

#include "Pi_Serial.h"
void Pi_Serial::serial_open(const char *path) {
    /***********************************************************
               打开串口
               打开的串口可读可写
               打开的串口不作为此进程的控制终端
               串口终端和控制终端不互相影响
               串口非阻塞
           ***********************************************************/
    fd = open(path, O_RDWR | O_NOCTTY | O_NONBLOCK);
    printf("path: %s\n", path);
    printf("fd: %d\n", fd);

    if(fd == -1 || fd == 0)
    {
        fprintf(stderr, "open() %s failed: %s\n",path, strerror(errno));
        exit(-1);
    }

    // 初始化串口
    /* init_tty(); */
    struct termios new_cfg, old_cfg;		//串口配置参数结构体
    if(tcgetattr(fd, &old_cfg) == -1)		//原终端参数保存在old_cfg
    {
        ;
        perror("tcgetattr() failed");
        return;
    }

    bzero(&new_cfg, sizeof(new_cfg));

    new_cfg = old_cfg;						//原有参数加入newcfg
    cfmakeraw(&new_cfg);					//设置串口参数为原始模式

    cfsetispeed(&new_cfg, B9600);			//设置串口输入波特率
    cfsetospeed(&new_cfg, B9600);			//设置串口输出波特率

    new_cfg.c_cflag |= CLOCAL | CREAD;		//本地连线，不具有数据机控制功能，能够读取数据

    new_cfg.c_cflag &= ~CSIZE;				//数据位掩码
    new_cfg.c_cflag |= CS8;					//8个数据位，1个终止位

    new_cfg.c_cflag &= ~PARENB;				//奇偶校验位失能
    new_cfg.c_cflag &= ~CSTOPB;				//停止位

    new_cfg.c_cc[VTIME] = 0;				//读取1个字符等待时间0 * (1/10)s
    new_cfg.c_cc[VMIN]  = 1;				//读取字符最少为1个
    tcflush(fd, TCIFLUSH);					//清空fd

    if(tcsetattr(fd, TCSANOW, &new_cfg) == -1)	//立即使能新的终端配置
    {
        close(fd);
        perror("tcsetattr() failed");

        return;
    }
}

const char* Pi_Serial::serial_close(int fd) {
    if(!close(fd)) {
        const char *ret = "关闭串口成功";
        return ret;
    }
    else {
        const char *ret = "关闭串口失败";
        return ret;
    }

}

void Pi_Serial::serial_write(char cmd[]) {
//    itoa(cmd, cmd_16, 16);
//    long long result=strtoll(cmd.c_str(), NULL, 16);
    write(fd, cmd, 5);
}
