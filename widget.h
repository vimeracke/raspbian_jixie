#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_cmd1_valueChanged(int value);

    void on_cmd2_valueChanged(int value);

    void on_cmd3_valueChanged(int value);

    void on_cmd4_valueChanged(int value);

    void on_cmd5_valueChanged(int value);

    void on_pushButton_1_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

private:
    Ui::Widget *ui;
public:

};

#endif // WIDGET_H
